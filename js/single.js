/* Share */
$("#vehicle_share_button").click(function (event) {
    event.preventDefault();
    $(this).toggleClass('opening');
    $(this).parent().toggleClass('expanded');
    // $(this).toggleText('✘', '');
});
// slider mầu sắc

// The slider being synced must be initialized first
// $('#vehicle_color_list').flexslider({
//     animation: "slide",
//     controlNav: false,
//     animationLoop: false,
//     slideshow: false,
//     itemWidth: 210,
//     itemMargin: 5,
//     asNavFor: '#slider'
// });

// $('#image').flexslider({
//     animation: "slide",
//     controlNav: false,
//     animationLoop: false,
//     slideshow: false,
//     sync: "#vehicle_color_list"
// });

function changeColor(value) {
    $.ajax({
        url: "./js/change_color.json",
        type: 'get',
        dataType: "json",
        beforeSend: function () {
            $('#color_loader').css('display', 'block');
        },
        success: function (result, status) {
            var urlImage = '';
            urlImage = result[value].urlImage;
            if (status == 'success') {
                $('#color_loader').css('display', 'none');
                console.log(urlImage);
                if (urlImage == '') {
                    console.log('Có lỗi xảy ra');
                } else {
                    $("#change_color").attr('src', urlImage);
                }
            } else {
                console.log('request failed');
            }
        }
    });
}